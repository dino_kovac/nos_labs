#include <unistd.h>
#include <cstdlib>
#include <signal.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#include <ncurses.h>

#include <iostream>
#include <list>
#include <vector>
#include <utility>
#include <string>

// size of the board
#define SIZE 5

// size of the message window
#define MESSAGE_LINES 7
#define MESSAGE_COLS 50

// order of start
#define FIRST 100
#define SECOND 200

// square types
#define UNKNOWN ' '
#define EMPTY '-'
#define OCCUPIED 'O'
#define HIT '#'
#define MISS 'x'

// message types
#define READY 1
#define BOOM 2
#define CONFIRM 3

struct _msg_ {
    long mtype;
    char mtext[100];
} typedef message;

char own_board[SIZE][SIZE];
char enemy_board[SIZE][SIZE];

int msqid;
key_t key = 1492;
message buffer;
int retval;
long starting;
short grid_width, grid_height;
short last_row = 0, last_column = 0;

std::list<std::string> messages;

WINDOW *own_window, *enemy_window, *message_window;

void create_boards() {

    std::vector<std::pair<int, int> > pairs;

    for(int i = 0; i < SIZE; ++i) {
        for(int j = 0; j < SIZE; ++j) {

            own_board[i][j] = EMPTY;
            enemy_board[i][j] = UNKNOWN;

            pairs.push_back(std::make_pair(i, j));
        }
    }

    for(int i = 0; i < SIZE; ++i) {
        int chosen = rand() % pairs.size();

        std::pair<int, int> p = pairs[chosen];
        own_board[p.first][p.second] = OCCUPIED;

        pairs.erase(pairs.begin() + chosen);
    }
}

void cleanup(int failure) {

    endwin();/* End curses mode  */

    if(msgctl(msqid, IPC_RMID, NULL) == -1) {
        exit(failure);
    } else {
        std::cout << "\nCleanup done. Exiting." << std::endl;
        exit(failure);
    }
}

void show_message(std::string message) {

    messages.push_back(message + "\n");

    if(messages.size() > MESSAGE_LINES - 2) messages.pop_front();

    werase(message_window);

    for(int i = 0; i < MESSAGE_COLS; ++i) wprintw(message_window, "-");

    for (std::list<std::string>::const_iterator it = messages.begin(), end = messages.end(); it != end; ++it) {
        wprintw(message_window, (*it).c_str());
    }

    for(int i = 0; i < MESSAGE_COLS; ++i) wprintw(message_window, "-");

    wrefresh(message_window);
}

void handshake() {

    // check if enemy ready
    retval = msgrcv(msqid, &buffer, sizeof(message) - sizeof(long), FIRST + READY, IPC_NOWAIT);
    if(retval == -1) {

        if(errno == ENOMSG) {
            starting = FIRST;
            show_message("P1: I'm ready. Are you there?");
        } else {
            perror("Error while checking if other player ready!");
            cleanup(-2);
        }

    } else {
        // other player is ready
        starting = SECOND;
        show_message("P2: I'm here. Let's do this!");
    }

    buffer.mtype = starting + READY;

    // send ready signal
    retval = msgsnd(msqid, &buffer, sizeof(message) - sizeof(long), 0);
    if(retval == -1) {
        perror("Error while sending ready message!");
        cleanup(-3);
    }

    if(starting == FIRST) {
        // wait until other player ready

        retval = msgrcv(msqid, &buffer, sizeof(message) - sizeof(long), SECOND + READY, 0);

        if(retval == -1) {

            perror("Error while waiting for other player to get ready!");
            cleanup(-4);
        }
    }
}

void draw() {

    wattron(enemy_window, A_BOLD|COLOR_PAIR(2));
    wattron(own_window, A_BOLD|COLOR_PAIR(2));

    for(int j = 0; j < SIZE; ++j) {
        wprintw(enemy_window, "+---");
        wprintw(own_window, "+---");
    }
    wprintw(enemy_window, "+");
    wprintw(own_window, "+");

    for(int i = 0; i < SIZE; ++i) {
        for(int j = 0; j < SIZE; ++j) {

            wattron(enemy_window, A_BOLD|COLOR_PAIR(2));
            wattron(own_window, A_BOLD|COLOR_PAIR(2));

            wprintw(enemy_window, "|");
            wprintw(own_window, "|");

            wattron(enemy_window, A_BOLD|COLOR_PAIR(1));
            wattron(own_window, A_BOLD|COLOR_PAIR(1));

            wprintw(enemy_window, " %c ", enemy_board[i][j]);
            wprintw(own_window, " %c ", own_board[i][j]);
        }

        wattron(enemy_window, A_BOLD|COLOR_PAIR(2));
        wattron(own_window, A_BOLD|COLOR_PAIR(2));

        wprintw(enemy_window, "|");
        wprintw(own_window, "|");

        for(int j = 0; j < SIZE; ++j) {
            wprintw(enemy_window, "+---");
            wprintw(own_window, "+---");
        }
        wprintw(enemy_window, "+");
        wprintw(own_window, "+");
    }

    wrefresh(enemy_window);
    wrefresh(own_window);
}

void update_board(short row, short column, WINDOW *win, char c) {

    wattron(win, A_BOLD|COLOR_PAIR(1));
    mvwprintw(win, row*2+1, column*4+2, "%c", c);
    wrefresh(win);
}

void init() {

    srand(time(NULL));

    msqid = msgget(key, 0600 | IPC_CREAT);
    if(msqid == -1) {
        perror("Error getting message queue!");
        cleanup(-1);
    }

    grid_width = 4*SIZE + 1;
    grid_height = 2*SIZE + 1;

    initscr();/* Start curses mode   */
    cbreak();     /* Intercept keyboard input */
    noecho();     /* Don't echo typed characters */
    keypad(stdscr,TRUE); /* Allow special keypad characters */

    start_color();

    init_pair(1, COLOR_GREEN, COLOR_BLACK);
    init_pair(2, COLOR_BLUE, COLOR_BLACK);

    enemy_window = newwin(grid_height, grid_width, 2, 2);
    message_window = newwin(MESSAGE_LINES, MESSAGE_COLS, grid_height + 4, 2);
    own_window = newwin(grid_height, grid_width, grid_height + MESSAGE_LINES + 6, 2);

    handshake();

    create_boards();

    draw();
}

void check_won() {

    retval = msgrcv(msqid, &buffer, sizeof(message) - sizeof(long), CONFIRM, 0);
    if(retval == -1) {

        perror("Error while waiting for confirmation!");
        cleanup(-5);
    }

    short row = buffer.mtext[0];
    short column = buffer.mtext[1];

    char hit = buffer.mtext[2];

    if(enemy_board[row][column] == UNKNOWN) {
        enemy_board[row][column] = hit ? HIT : MISS;
        update_board(row, column, enemy_window, enemy_board[row][column]);
    }

    if(hit) {
        show_message("DIRECT HIT!");
    } else {
        show_message("MISS!");
    }

    if(buffer.mtext[3]) {
        show_message("VICTORY! Press any key to exit...");
        wgetch(message_window);
        cleanup(0);
    }
}

int check_lost() {

    for(int i = 0; i < SIZE; ++i) {
        for(int j = 0; j < SIZE; ++j) {
            if(own_board[i][j] == OCCUPIED) return 0;
        }
    }

    return 1;
}

void get_coordinates() {

    wmove(enemy_window, 2*last_row + 1, 4*last_column + 2);
    wrefresh(enemy_window);

    char ch;

    do {

        ch = wgetch(enemy_window);

        switch(ch) {
        case 67: // ARROW_RIGHT
            if(last_column < SIZE - 1) ++last_column;
            break;
        case 68: // ARROW_LEFT
            if(last_column > 0) --last_column;
            break;
        case 66: // ARROW_DOWN
            if(last_row < SIZE - 1) ++last_row;
            break;
        case 65: // ARROW_UP
            if(last_row > 0) --last_row;
            break;
        }

        wmove(enemy_window, 2*last_row + 1, 4*last_column + 2);
        wrefresh(enemy_window);

    } while (ch != 'q' && ch != '\r' && ch != ' ');

    if(ch == 'q') cleanup(0);
}

void fire() {

    show_message("Your move.");

    get_coordinates();

    buffer.mtype = BOOM;
    buffer.mtext[0] = last_row;
    buffer.mtext[1] = last_column;

    retval = msgsnd(msqid, &buffer, sizeof(message) - sizeof(long), 0);
    if(retval == -1) {
        perror("Error while firing!");
        cleanup(-6);
    }
}

void duck() {

    retval = msgrcv(msqid, &buffer, sizeof(message) - sizeof(long), BOOM, 0);
    if(retval == -1) {

        perror("Error while waiting for confirmation!");
        cleanup(-5);
    }

    short row = buffer.mtext[0];
    short column = buffer.mtext[1];

    char hit = own_board[row][column] == OCCUPIED ? 1 : 0;
    if(own_board[row][column] != HIT && own_board[row][column] != MISS) {
        own_board[row][column] = hit ? HIT : MISS;
        update_board(row, column, own_window, own_board[row][column]);
    }

    char lost = check_lost();

    buffer.mtype = CONFIRM;
    buffer.mtext[2] = hit;
    buffer.mtext[3] = lost;

    retval = msgsnd(msqid, &buffer, sizeof(message) - sizeof(long), 0);
    if(retval == -1) {
        perror("Error while sending confirmation!");
        cleanup(-6);
    }

    if(lost) {
        show_message("GAME OVER! You lost. Press any key to exit...");
        wgetch(message_window);
        cleanup(0);
    }
}

void fight() {

    short my_turn = 0;

    if(starting == FIRST) {
        my_turn = 1;
    }

    while(1) {

        if(my_turn) {

            fire();
            check_won();
        } else {

            duck();
        }

        my_turn = !my_turn;
    }
}

int main() {

    sigset(SIGINT, cleanup);

    init();

    show_message("Let the games begin!");
    show_message("Move with arrows.");
    show_message("Shoot with SPACE.");

    fight();

    cleanup(0);

    return 0;
}
