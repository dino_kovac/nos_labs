#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <iostream>
#include <string>
#include <sstream>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>

#define MAXREAD 100

int main() {

	int pfd[2];
	int inpipe, outpipe;
	char buf[MAXREAD] = "";
	char npipe[] = "./pipe";

	char ch;
	std::string text;
	std::stringstream ss;

	do {

		std::cout << "(e)ncrypt, (d)ecrypt or (q)uit: ";
		ch = getchar();

		if(ch == 'd' || ch == 'e') {
			std::cout << "[C] Enter text: ";
			std::cin >> text;
		}

		if(ch == 'e') {

			if (pipe(pfd) == -1) {
				perror("pipe() failed! Aborting.");
				exit(1);
			}

			switch (fork()) {

				case -1:
					perror("fork() failed! Aborting.");
					exit(1);

				case 0: // child

					if((outpipe = dup(pfd[1])) == -1) {
						perror("dup() failed! Aborting.");
						exit(1);
					}

					close(pfd[1]);
					(void) read(pfd[0], buf, MAXREAD);
					std::cout << "[E] got text: " << buf << std::endl;

					close(pfd[0]);
					ss << "kriptirano(" << buf << ")" << std::endl;
					ss >> text;
					(void) write(outpipe, text.c_str(), strlen(text.c_str()) + 1);
					close(outpipe);
					exit(0);

				default: // parent

					if((inpipe = dup(pfd[0])) == -1) {
						perror("dup() failed! Aborting.");
						exit(1);
					}

					close(pfd[0]);
					(void) write(pfd[1], text.c_str(), strlen(text.c_str()) + 1);

					close(pfd[1]);
					wait(NULL);

					(void) read(inpipe, buf, MAXREAD);
					close(inpipe);
					std::cout << "[C] got back text: " << buf << std::endl << std::endl;
			}

		}

		if(ch == 'd') {

			unlink(npipe);

			if(mknod(npipe, S_IFIFO | 00600, 0) == -1) {
				perror("mknod() failed! Aborting.");
				exit(1);
			}

			switch (fork()) {

				case -1:
					perror("fork() failed! Aborting.");
					exit(1);

				case 0: // child

					if((inpipe = open(npipe, O_RDONLY)) == -1) {
						perror("open() failed! Aborting.");
						exit(1);
					}

					(void) read(inpipe, buf, MAXREAD);
					std::cout << "[D] got text: " << buf << std::endl;

					close(inpipe);
					ss << "dekriptirano(" << buf << ")" << std::endl;
					ss >> text;

					if((outpipe = open(npipe, O_WRONLY)) == -1) {
						perror("open() failed! Aborting.");
						exit(1);
					}

					(void) write(outpipe, text.c_str(), strlen(text.c_str()) + 1);
					close(outpipe);
					exit(0);

				default: // parent

					if((outpipe = open(npipe, O_WRONLY)) == -1) {
						perror("open() failed! Aborting.");
						exit(1);
					}

					(void) write(outpipe, text.c_str(), strlen(text.c_str()) + 1);

					close(outpipe);

					if((inpipe = open(npipe, O_RDONLY)) == -1) {
						perror("open() failed! Aborting.");
						exit(1);
					}

					(void) read(inpipe, buf, MAXREAD);
					close(inpipe);
					std::cout << "[C] got back text: " << buf << std::endl << std::endl;

					wait(NULL);
			}

		}

	} while (ch != 'q');

	std::cout << "Bye." << std::endl;

	exit(0); // closes all descriptors
}